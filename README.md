# Spinster Moderator Handbook

[![Build Status](https://gitlab.com/spinster-xyz/handbook/badges/master/build.svg)](https://handbook.spinster.xyz/)

---

An extension of the [Spinster rules](https://spinster.xyz/about), the Moderator Handbook explains how Spinster interprets those rules and takes action.

It's primarily directed towards moderators and staff, but publicly visible to all for transparency.

If you think our process should change, anyone is free to open an issue or pull request.

## License

Spinster's Moderator Handbook is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

The handbook is free for everyone to copy and share.
If you'd like to use some or all of it for your own community, please do so without asking for permission.
All we ask is that you continue to pass on this freedom to others.
