# Mod Expectations

* Must be a woman (18+ female)
* Must be verified by ID and use real name on Spinster
* Must be able to commit a minimum of 5 hours per week
* Must join and participate in Spinster Mod Riot chatroom
* Must state that you are a Mod in your Spinster profile (we will provide a Mod badge)

## Moderator Code of Conduct

Moderators are held to a higher standard of behavior since they serve as representatives of the Spinster community. We expect Moderators to not instigate drama, make public posts that would paint the community in a negative light, or attack other feminists beyond respectful disagreement.

Behavior that puts the success or goals of Spinster at risk is not acceptable by Moderators, and is grounds for removal.

## Responding to mobs

The only way to break the butthurt-reaction cycle is to not react. When someone is attacking you, do not react. When they are deliberately mischaracterizing what you say, do not react. If they are arguing in good faith, you can have an interesting discussion. But you know when it’s not good faith. And you can tell when they smell blood and start to form a mob. DO NOT REACT. Any reaction from you is like a drug to them, inflaming their frenzy. If you are feeling emotions like urgency, despair, or anger, step away from the computer for a few hours or days or weeks.In terms of communicating, consider what statement you made that people are supposedly angry about. Is it true? Is it clear? That’s usually the case. Online mobs are especially provoked by truth and clarity (example: “if a person has a penis he’s a man.”) If your instigating statement was neither true nor clear, you can clear it up later, after you’ve had a break. But the mob will be pressuring you to apologize for truth and clarity. Don’t do that. Let them rage. The rage is on them.
