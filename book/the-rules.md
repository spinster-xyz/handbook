# The Rules

## 1. No hate speech

> We support open and civil dialogue on social and political issues, but do it respectfully. Moderators will use their discretion.

This includes, but is not limited to, extreme racism, sexism, and homophobia.

We don’t take these terms lightly and don’t want to police differences of opinion made in good faith, even if it relates to sex, race, or orientation. We consider this rule a catch-all for the most egregious behavior, rather than a tool to create a “safe space”. We are NOT a safe space, and users should expect disagreement.

We do not consider a statement of verifiable fact in-and-of-itself to be hate speech (even if the fact itself is in dispute) just because it is related to a targeted group.

We also realize that radical feminists may see transgender ideology as inherently misogynistic or homophobic. However, we won’t be moderating pro-transgender comments unless they contain explicit hate speech towards a group of people.

### Context matters

When determining if content violates this rule, we should take the following pieces of context into account:

1. Does the content carry historic weight of discrimination, or is regularly accompanied by threats of violence towards a particular group of people? (ex. Use of the n-word towards a person of color)

2. If related to a specific word, is the word being used by a member of the group at which it is normally directed in an attempt at reclaiming, irony, or humor? Or is the word being used in art, especially by the group at which it is normally directed? (ex. A woman reclaiming the word “cunt”)

3. Is the content being used to educate about the discrimination that a particular group has faced? (ex. Videos from the Holocaust)

### Examples

Examples of posts that would **not** be considered hate speech:

* *“Women should shave because no one wants to see a woman with a legbeard!”*
* *“African countries have lower IQs than white countries.”*
* *“Gloria Steinem, leader of the women’s movement, was Jewish!”*
* *“Political lesbians are not real lesbians. They are straight!”*
* *“Bisexuals need to get their shit together. Are you with us or not?”*

Examples of posts that **would** be considered hate speech:

* *“Women are property.”*
* *“Black apes are so dumb, they should go back to Africa.”*
* *“Jewish feminists are disgusting - they are out to destroy white America!”*
* *“Lesbians like you can shut up and suck my dick.”*
* *“Bisexuals are all filthy, disgusting liars. Never trust one!!!”*

## 2. No threats of violence or glorification of violence

> This includes but is not limited to: pro-Nazi propaganda, white nationalist content, threats against women for disagreeing with you, and any promotion of violence, including against non-human animals.

This tends to be pretty straightforward. The above items are just examples. Glorification of any form of violence, even towards an oppressor class, is not allowed. Sharing the reality of a violent event is not considered promotion or glorification unless it is met with further commentary which does so.

Encouraging self harm or suicide will be moderated under this rule. Requests for help regarding mental health or suicidal thoughts will not be.

### Examples

For example, we would **not** moderate:

* *“All rapists need to be thrown in jail for the rest of their lives.”*
* *“This person was beaten for using the wrong bathroom in Liverpool.”*

We **would** moderate:

* *“Women need to start castrating rapists.”*
* *“This person was beaten for using the wrong bathroom - we need to see more of this!”*

### Violence against animals

Violence against animals tends to be a little bit more of a gray area. While the animal agriculture industry, as with many other industries, are inherently violent, we do not want to stifle civil discussion around these issues. In this case, we only moderate content which actively, intentionally, and blatantly advocates for violence against animals.

For example, we would **not** moderate:

* *“I had a cheeseburger for lunch and it was so good!”*
* *“I don’t like how animals are treated, but I really don’t see anything wrong with eating them, inherently.”*

We **would** moderate:

* *“I don’t care if cows are raped. They are not sentient and don’t matter.”*
* *“I”M GOING TO GO KILL A PIG NOW! MMM BACON!”*

Any depictions of graphic violence for educational purposes should be behind a Content Warning (see rule 7)

Artistic material which contains violence, but is not a glorification, is not a violation (but should probably be behind a CW if it’s so bad you had to ask). Artistic material which does glorify or promote violence basis will be handled on a case-by-case basis depending on the severity of the violence, the seriousness with which is described, and the in-universe context.

## 3. No doxxing, stalking, or harassment

> We may take your behavior offsite into account when assessing this. For example, posting uncensored, private, screenshots to another site with the intent to harass users. We may also include incitement to any of the below.

### Doxxing

Doxxing **is not…**

* Posting screenshots of a public post to another website

* Talking about someone who could be identified from the post

* Posting information that is already public, especially if it was intentionally made public by the relevant party (ex. a work address or email that is on their website)

Doxxing **is…**

* Posting private, personal, details about a person which puts them at risk (ex. home address, private phone numbers, and private emails), especially when they did not consent to the initial publication of this information

* Identifying the legal name of someone who has wished to remain anonymous

### Stalking

Stalking is a pattern of unwanted behavior, especially with the intent to instill fear or cause harm. Stalking may include (but is not limited to), unwanted messages, tagging, liking, following, etc. Behavior on other sites or IRL may be taken into account when moderating for stalking. Stalking may also include incitement to stalk.

### Harassment

Harassment **is not…**

* Disagreeing, arguing, or repeatedly responding to public posts

* Saying mean words, insults, or hurtful comments that don’t break another rule

Harassment **is…**

* A pattern of repeated behavior intended to cause distress, especially after being asked to stop

A lot of users get into back and forth spats, and then report each other for harassment. If both parties are actively participating in the spat, this is not harassment - even if one person is being much more rude or cruel than the other. It would rise to the level of harassment once one person asks the other to stop, and the other continues -- especially if they continue multiple times after the first party has stopped. If they only continue once after the other person has stopped, I would leave that alone unless it’s a very severe comment. Harassment may also be entirely one-sided, such as @-ing someone out of the blue, multiple times, to say something cruel to them.

Since harassment is a pattern of behavior, the actual content of individual posts is less important than the way in which they have been delivered. If someone reports a single post for harassment, it is worth it to go back and look at the context and entire post history of the reported user. Often, someone experiencing harassment only reports the most recent post, obscuring the fact that there has been a pattern. The content of the posts should be considered to determine the severity of harassment, and what course of action is necessary (ex. warning, deletion, suspension). Harassment may also include incitement to harass.

Impersonation that is not clearly communicated as parody may be considered harassment.

## 4. No content that is illegal for us to host in the United States

> Links to illegal content on other sites will be handled on a case-by-case basis.

Violations **would** include (but are not limited to):

* Uploading material that you do not have the copyright to (we will only remove copyrighted material with a DMCA request)

* Arranging the buying/selling of illegal substances

* Facilitation of sex trafficking

Violations would **not** include:

* Links to off-site pirated material

* Discussing personal use of illegal drugs

## 5. No pornography of any kind

> This includes, but is not limited to, content for the sole purpose of sexually objectifying women, or for participation in the sale of another human for sex.

This does not include nude art (especially non-photography), depictions of women’s sexuality for educational purposes, or celebrations of women’s anatomy from a feminist perspective.

We do not consider breasts inherently pornographic, unless framed for the male gaze.

Photos of minors are subject to stricter moderation. If the subject of a sexualized photo appears to be under age, even if it is not actually pornographic, we will remove it.

## 6. NSFW must be behind a Content Warning

> NSFW = "Not Safe for Work", and includes nudity, or anything exceptionally vulgar or graphic - even if shared for educational purposes.

Pretty much anything listed above as “not porn” should be behind a content warning. Graphic depictions of violence, including rape, should be as well.

We do not expect all mentions of rape, sexual assault, mental health issues, etc, to be behind a content warning unless it is exceptionally graphic. However, people are free to use the CW feature at their discretion.

## 7. No Spam

This includes repeatedly posting the same or similar message in a short period of time, posting the same link over and over again without good reason, linking to malicious sites, etc. A pattern of intentionally malicious false reporting may be considered spam.

## 8. Must be 13+ to join

We have no real way to enforce this, but if we find out you are under 13 somehow we will kick you out.
