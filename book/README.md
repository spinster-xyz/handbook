# Spinster Moderator Handbook

![Spinster Mods](images/spinster-mod-icon.png)

*If you believe there has been an abuse of moderator power, please email: [abuse@spinster.xyz](mailto:abuse@spinster.xyz) with details, including any screenshots you can provide.*

## Table of Contents

* [Introduction](README.md)
* [Mod Expectations](mod-expectations.md)
* [Spirit of Spinster](spirit-of-spinster.md)
* [The Rules](the-rules.md)
  * [1. No hate speech](the-rules.md#1-no-hate-speech)
  * [2. No threats of violence or glorification of violence](the-rules.md#2-no-threats-of-violence-or-glorification-of-violence)
  * [3. No doxxing, stalking, or harassment](the-rules.md#3-no-doxxing-stalking-or-harassment)
  * [4. No content that is illegal for us to host in the United States](the-rules.md#4-no-content-that-is-illegal-for-us-to-host-in-the-united-states)
  * [5. No pornography of any kind](the-rules.md#5-no-pornography-of-any-kind)
  * [6. NSFW must be behind a Content Warning](the-rules.md#6-nsfw-must-be-behind-a-content-warning)
  * [7. No Spam](the-rules.md#7-no-spam)
  * [8. Must be 13+ to join](the-rules.md#8-must-be-13-to-join)
* [Moderation Practices](moderation-practices.md)
  * [Local vs. remote users](moderation-practices.md#local-vs-remote-users)
  * [Delete, silence, disable, and suspend](moderation-practices.md#delete-silence-disable-and-suspend)
  * [Recording actions](moderation-practices.md#recording-actions)
  * [Working as a team](moderation-practices.md#working-as-a-team)
  * [Requests for information](moderation-practices.md#requests-for-information)
