# Moderation Practices

## Local vs. remote users

Local users are users who have signed up on Spinster. Remote users are users who have signed up on any of the other thousands of servers with which we may federate. You can tell if a user is local or remote by looking at their full handle.

Local User:
![Local user](images/image10.png)

Remote User:
![Remote user](images/image5.png)

In the moderation panel, a local user will not have their full domain displayed, while a remote user will.

When we moderate remote content, this does not actually impact the user’s post or existance on their own local server. Because of this, we have different standards of moderation for local vs. remote users.

### Remote users

When moderating remote users and content, use a very liberal interpretation of the rules. If it appears to be a violation, err on the side of moderating. If the user has clear violations of our rules in their history, feel free to suspend them without warning. The reason for this is because there are essentially no real consequences to the user beyond being denied access to Spinster users, who mostly don’t want to engage with that content anyways.

### Local users

We treat content moderation for local users the opposite. We are very conservative with what we will delete, and it must be a clear violation of our rules. If you are not sure if it’s a violation - either ask other mods, or leave it. We are also very generous with assuming good intent with our users, and will often give multiple warnings for minor violations before serious action is taken. Remember: this site was founded on the principle that “we don’t ban feminists”, so banning a local user requires fairly egregious abuse or a pattern of hostile rule violations.

## Delete, silence, disable, and suspend

There are 3 main actions that can be taken on a report: Delete, Silence, and Suspend. These are used differently depending on the severity of the violation, past violations, and if they are a local or remote user.

### Delete

Deleting posts is the most common action taken against local users. If a specific post violates our rules, we are able to delete it from the Mod panel. The is almost always used for local users, as remote users who post violating content should just be suspended. When you delete a post, that post is permanently gone, and can not be restored. Replies to the post itself remain, but become orphaned (detached from the thread).

Delete a post by checking the box next to the post in the moderation panel, and clicking “Delete”

![Delete post](images/image6.png)

If a post is not inherently a violation, but there is media attached which should have been behind a Content Warning, you can click “Mark as Sensitive” instead of deleting the post entirely.

If you delete or mark a local user’s post as sensitive, DM the user and let them know what action you took and why, using the template below, attach a screenshot of the post, and name rule that was violated. Please CC MK on these messages by including @mk anywhere in the message.

> Hi @[USERNAME] @mk -
>
> The following post has been deleted for violating our rules against [RULE]. Please do not continue to [DO THE THING]. You can review our rules here:
>
> https://spinster.xyz/about
>
> Thanks!  
> \- [MOD_NAME]

![Direct message example](images/image1.png)

Sometimes, depending on context, you may want to add an extra note. For example, on posts that are not malicious in nature but just over the line, you might point out:

> I realize you were being ironic, however this joke is just over the line so unfortunately we can’t permit it.

You can also tell the user what they can do to be in line with the rules, such as reposting the content without the specific line or offensive word. If this user has already received a warning on this topic, this should also be mentioned to them at this time.

### Silence

Silencing is essentially a shadow ban. People can still follow this user to choose to see their content and receive notifications from them, however users who do not follow the silenced user will not see their content or receive notifications from them.

We may use this tool to deal with remote trolls who are annoying, but not necessarily breaking our rules. For example, men who repeatedly come from kiwifarms to Spinster to argue with lesbians may find themselves silenced.

However, we use this tool incredibly sparingly on local users. There are very rare cases in which silencing a local user is appropriate. Either they are a user in good standing and have the same rights as everyone else, or they are violating rules and should be suspended.

Exceptions to this include trolls which we feel would be better silenced than suspended and returning under a new account, or potentially disruptive bots. In the future we may find other uses for silencing, but for now this is it. If you decided to silence a local user, please run it by another mod first to ensure this is the best course of action.

### Suspend

Suspending means two different things for local vs remote users.

**Remote Suspension:** The remote user is no longer able to interact with Spinster users, and all of their content is blocked from Spinster feeds. Previous posts of theirs will become unavailable to Spinster users, and will appear as if deleted. However, their posts and account remain intact on their own server. The account can be “unsuspended” - which will allow them to interact with our server again.

**Local Suspension:** All data associated with the users account, besides their username and account status, are irrevocably deleted. The account can be “unsuspended” - but this only re-enables the user to login. All data that was deleted is still lost, and can only be restored from a database backup.

Because suspending a local user is a serious and irrevocable action, we only do this in extreme cases, especially for active local users who are engaging in good faith.

Actions that would warrant suspension include:

* Obvious bad faith troll/spamming - such as creating fake accounts in order to spam the same message on every post

* Someone who has created a new account to get around suspension, blocks, or other limitations on a different account

* Repeated and serious violations of our rules, after multiple warnings

For example, we would **not** suspend someone for:

* Posting multiple over the line memes and stopping when asked

* One or two violations of a rule, such as slurs, harassment, etc.

* Violations of multiple different rules in unrelated instances

We would **consider** suspending someone for:

* Repeat spamming

* Multiple violent posts

* Instances of serious known behavior offsite - for example a domestic abuser using Spinster to stalk his ex, even if it was just one message

* Repeat instances of harassment, especially if coupled with other rule violations (such as racism, homophobia, etc)

* Refusal to change behavior to align with rules after multiple warnings
 
* Using alternate accounts, either local or remote, to get around either personal blocks or moderation (this will always be considered a bannable violation, regardless of the severity of the initial violation)

![Suspending a user](images/image9.png)

When suspending, **always** double check that you are suspending the correct user. Since unlike silencing this can not truly be undone, it is incredibly important to not make this mistake -- and easier than you may think to do so.

We have decided that only admins will be able to suspend a user for now - so if you believe someone should be suspended, Mods should disable their login (see next section) and suggest suspension to Alex and MK.

### Disable Login

Disable login essentially apprehends the user while we decide what to do with them. It will log them out on all devices, and prevent them from logging back in until we allow it. Mod should do this to anyone they feel needs to be suspended, and then recommend suspension to the Alex and MK (use the mod chat for now).

The easiest way to disable login is to click the username on the report, and then view the mod panel for that user:

![Report panel](images/image3.png)

Scroll down, and click Disable:

![Disable user](images/image11.png)

Please include the following message when disabling a users login:

**Disable email template:**

> Your account is being temporarily disabled due to multiple or severe violations of our rules against [BROKEN_RULE] and may be suspended pending admin review. If you feel this decision has been made in error, please email: abuse@spinster.xyz

## Recording actions

When you receive a report, it will look pretty much like this:

![Report](images/image7.png)

When you take action on a report, you should leave a comment on the report outlining what action you took and why. This way, other mods will be able to view relevant context. This is especially important if you delete a post, as the deleted post will not be visible in the future, so mods will not be able to see if they have violated these rules in the past without that context.

![Account notes](images/image2.png)

You can view all actions that have been taken at: https://spinster.xyz/admin/action_logs

![Action logs](images/image4.png)

Be sure to mark a report as “Resolved” when you have finished with it, even if you decided not to take any action:

![Mark resolved](images/image8.png)

## Working as a team

If you are handling a report outside of your assigned office hours, please write in the mod chat that you are handling the report like: *“Hey, I’ve got #980 under control.”* This way, mods won’t step on each other’s toes. Until we assign office hours, **please always do this.**

If you are unsure about an action, please feel free to run it by another mod. Our mod team contains diverse opinions on free speech, censorship, and safety - so try to ask someone who has a different opinion than you.

If you feel a mod has made a mistake, please speak to her directly first before talking to Alex or MK about it and give them a chance to own up to the mistake on their own. If a mistake has been made, we can agree on actions for accountability, transparency, and reparation as necessary. For example, MK accidentally suspended the wrong person once -- so we agreed to make a public announcement, restore her account, give her Pro for 5 years, and help her regain her followers through boosting, shoutouts, etc.

## Requests for information

Moderators should not respond to requests for information on how a report was handled or if a user has deleted their own account or content, but rather direct users to [abuse@spinster.xyz](mailto:abuse@spinster.xyz) if they would like an admin to review a moderator’s behavior.

Users may request follow up information on reports they directly filed, which may be responded to on a case-by-case basis, especially if they were the primary target of an attack.

In the case of public requests for info on a moderation action you may use this template:

> Hi! We can't discuss moderation actions taken against a user, or privacy actions taken by the user themselves, publicly as this is a violation of our privacy policy. However, if you filed a report you are welcome to follow up with a request on how it was handled, and we will try to respond. In the meantime, feel free to review our moderation handbook here, which may provide some insight:
>
> https://handbook.spinster.xyz

**Stop reporting random remote users template:**

> Hi @[USER_NAME] -
>
> I noticed you were reporting a lot of Remote users (not Spinster accounts). If they are not directly interacting with Spinster, they are usually best dealt with by blocking or muting. The button with three little dots below every post, next to the star, will give you a “block” option in a pulldown menu if you click it. Please block liberally!
>
> If you’re seeing a lot of posts from remote users, you may be in the “Fediverse” tab rather than the “Spinster” tab. “Fediverse” shows all kinds of users, many of whom are awful; silencing all of them is a sisyphean task. The “Spinster” tab will show just Spinster accounts, and give you a higher quality feed if that’s what you’re looking for.
>
> For future reporting, please see Spinster’s rules at https://spinster.xyz/about
