# The Spirit of Spinster

Spinster was created to solve two problems:

1. Lack of balanced public discourse on “gender identity” due to the silencing of women/feminists on most social media platforms, including the Fediverse.

2. The corporate control over our communication in general, as a global population, through the use of centralized, proprietary, software.

Our rules are designed to help us solve those problems by creating a space where this discussion is able to happen outside of the realm of corporate control. When moderating content on Spinster, decisions should be made through the lens of how they impact both of these goals.

We use local moderation conservatively, preferring to allow users to self-moderate their timeline through their personal moderation preferences. Moderation should only take place in the occurrence of a clear rule violation. Spinster is not a “safe space” any more than Twitter is. While some moderation exists to protect individual users, moderation overall is to protect the site itself. We are primarily removing content that puts our server at risk of violating our host’s Terms of Service, rather than moderating content that hurts users feelings or offends. Some user-centric moderation is required, though, to allow users to feel comfortable enough to participate on Spinster and to have a positive experience. Most rules serve both goals.
